<?php
require_once "const.php";
require_once "cors.php";
cors();
header('Content-Type: text/html; charset=UTF-8');

$userId = htmlspecialchars($_GET["userId"]);
$trace = $_GET["trace"];

if ($trace != 1) {
    // Désactiver le rapport d'erreurs
    error_reporting(0);
}

require_once WORDPRESS_DIR . "/wp-load.php";

$result = wp_delete_user($userId, 62);
if ($result == 1) {
    $response["success"] = 1;
} else {
    $response["success"] = 0;
}

require_once "http_helper.php";
sendJsonResponse($response, $trace);
?>
