<?php

function sendJsonResponse($response, $trace)
{
    $flags = JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_UNESCAPED_SLASHES;

    $result = json_encode($response, $flags);

    if ($trace == 1) {
        require_once "jsonHelper.php";
        $result = prettyPrint($result);
    }

    if ($trace == 1) {
        echo "<pre>";
        print_r($result);
        echo "</pre>";
    } else {
        echo $result;
    }
}

?>