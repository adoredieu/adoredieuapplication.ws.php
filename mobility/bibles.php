<?php

function curPageURL()
{
    $pageURL = 'http';
    if ($_SERVER["HTTPS"] == "on") {
        $pageURL .= "s";
    }
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["SCRIPT_NAME"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["SCRIPT_NAME"];
    }
    return $pageURL;
}

function getDirectoryEntries($path, $trace)
{
    $result = array();
    $index = 0;

    if ($handle = opendir($path)) {
        while (false !== ($entry = readdir($handle))) {
            if ($entry != "." && $entry != "..") {
                $result[$index] = $entry;
                $index++;
            }
        }
        closedir($handle);
    }

    if ($trace == 1) {
        echo "Found $index entries in $path</br>";
        print_r($result);
    }

    return $result;
}


function findMaximumBibleVersion($entries, $trace)
{
    $result = -1;

    foreach ($entries as $entry) {
        if ($trace == 1) {
            echo "Entries $entry</br>";
        }

        if (is_numeric($entry) && intval($entry) > $result) {
            $result = intval($entry);
        }
    }

    return $result;
}

header('Content-Type: text/html; charset=UTF-8');

$trace = $_GET["trace"];
$mirror = $_GET["mirror"];

if ($trace != 1) {
    // Désactiver le rapport d'erreurs
    error_reporting(0);
}

$lucene_version = $_GET["lucene_version"];
if (empty($lucene_version)) {
    $luceneDirName = dirname(__FILE__) . "/bibles/";
    $luceneDir = getDirectoryEntries($luceneDirName, $trace);
    $lucene_version = $luceneDir[0];
}

$directory = dirname(__FILE__) . "/bibles/$lucene_version/";
if (is_dir($directory)) {
    $filenames = getDirectoryEntries($directory, $trace);
    // do something
} else {
    echo "No such directory";
    exit(1);
}

if ($trace == 1) {
    echo "<pre>";
    echo "dir : $directory</br>";
    echo "files : ";
    print_r($files);
    echo "</br>";
}
$version = findMaximumBibleVersion($filenames, $trace);

if ($version > 0) {
    $result["lucene_version"] = $lucene_version;
    $result["version"] = $version;
    if ($mirror == 1) {
        $result["zipUrl"] = dirname(curPageURL()) . "/bibles/$lucene_version/" . $result["version"] . "/data.zip";
    } else {
        $result["zipUrl"] = "https://www.adoredieu.org/mobility/bibles/$lucene_version/" . $result["version"] . "/data.zip";
    }

    echo json_encode($result, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_UNESCAPED_SLASHES);
}

if ($trace == 1) {
    echo "</pre>";
}
