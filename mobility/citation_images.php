<?php
require_once "cors.php";
cors();
date_default_timezone_set('Europe/Paris');
header('Content-Type: text/html; charset=UTF-8');

$getLastDate = $_GET["getLastDate"];
$page = $_GET["page"];
$item_per_page = $_GET["items_per_page"];
$trace = $_GET["trace"];

if ($trace != 1) {
    // Désactiver le rapport d'erreurs
    error_reporting(0);
}

require_once "const.php";
require_once("PHP_DatabaseLib/MysqlProxyPhp7.php");

$mysqlProxy = new MysqlProxy(DATABASE_SERVER, DATABASE_USER, DATABASE_PASSWORD, CONTENT_DATABASE_NAME);
$mysqlProxy->Connect();

$query = 'SET CHARACTER SET utf8';
$mysqlProxy->ExecuteQuery($query);

$result = "";

if ($getLastDate == 1) {
    $query = "SELECT `post_date` "
        . "FROM " . DATABASE_PREFIX . "posts "
        . "WHERE post_type='" . CITATION_IMG_CAT_ID . "' AND post_date<='" . date("c") . "' "
        . "AND post_title!='Brouillon auto' "
        . "ORDER BY post_date DESC LIMIT 1";

    if ($trace == 1) {
        echo $query . "<br />";
    }

    $query_result = $mysqlProxy->ExecuteQuery($query);
    $lastCitation = $mysqlProxy->FetchAssoc($query_result);
    $result = $lastCitation["post_date"];
} else {
    if (empty($page)) {
        $page = 0;
    }
    if (empty($item_per_page)) {
        $item_per_page = 5;
    }

    $start_from = $page * $item_per_page;
    $query = "SELECT `id`, `post_title`, `post_date`, `post_name` "
        . "FROM " . DATABASE_PREFIX . "posts "
        . "WHERE post_type='" . CITATION_IMG_CAT_ID . "' AND post_date<='" . date("c") . "' "
        . "AND post_title!='Brouillon auto' "
        . "ORDER BY post_date DESC LIMIT " . $start_from . ", " . $item_per_page . "; ";

    if ($trace == 1) {
        echo $query . "<br />";
    }

    $query_result = $mysqlProxy->ExecuteQuery($query);

    $articles = $mysqlProxy->FetchAll($query_result);

    $citationsImagesFormatted = null;
    $i = 0;
    foreach ($articles as $article) {
        $query = "SELECT `meta_value` FROM " . DATABASE_PREFIX . "postmeta 
        WHERE post_id=(SELECT `meta_value` FROM " . DATABASE_PREFIX . "postmeta WHERE post_id=" . $article["id"] . " AND meta_key = '_thumbnail_id') 
        AND meta_key='_wp_attached_file';";

        $query_result = $mysqlProxy->ExecuteQuery($query);
        $metaData = $mysqlProxy->FetchRow($query_result);

        if ($trace == 1) {
            echo $query . "<br />";
        }

        $citationsImagesFormatted[$i]["id"] = $article["id"];

        $citationsImagesFormatted[$i]["publish_up"] = $article["post_date"];

        $citationImageLink = CITATIONS_IMAGES_BASE_LINK . $article["post_name"] . '/';
        $citationsImagesFormatted[$i]["article_link"] = $citationImageLink;

        $trimmedTitle = trim(strip_tags($article["post_title"]));
        $citationsImagesFormatted[$i]["title"] = $trimmedTitle;
        $citationsImagesFormatted[$i]["introtext"] = $trimmedTitle;

        $citationsImagesFormatted[$i]["thumbnail"] = null;
        if ($metaData[0] != null) {
            $citationsImagesFormatted[$i]["thumbnail"] = DOMAIN_NAME . "/wp-content/uploads/" . $metaData[0];
        }

        $citationsImagesFormatted[$i]["facebook_link_html"] = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN\" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <script type="application/javascript" src="facebook_sdk.js"></script>
</head>
<body>
<div id="fb-root"></div>
<div class="fb-like" data-href="' . $citationImageLink . '" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
</body>
</html>';
        $citationsImagesFormatted[$i]["fb_article_link"] = $citationImageLink;

        $i++;
    }

    $flags = JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_UNESCAPED_SLASHES;

    $result = json_encode($citationsImagesFormatted, $flags);
    if ($trace == 1) {
        require_once "jsonHelper.php";
        $result = prettyPrint($result);
    }
}

$mysqlProxy->Disconnect();

if ($trace == 1) {
    echo "<pre>";
    print_r($result);
    echo "</pre>";
} else {
    echo $result;
}

?>
