<?php
require_once "cors.php";
cors();
header('Content-Type: text/html; charset=UTF-8');

$trace = $_GET["trace"];
$version = $_GET["version"];
$versesHighlightsSynchronisation = $_POST["versesHighlightsSynchronisation"];

if (empty($versesHighlightsSynchronisation))
    return;

if ($trace != 1) {
    // Désactiver le rapport d'erreurs
    error_reporting(0);
}

require_once "const.php";
require_once "strings_helper.php";
require_once("PHP_DatabaseLib/MysqlProxyPhp7.php");

$decodedJson = json_decode($versesHighlightsSynchronisation, true);

$mysqlProxy = new MysqlProxy(DATABASE_SERVER,
    DATABASE_USER,
    DATABASE_PASSWORD,
    MOBILITY_DATABASE_NAME);

$mysqlProxy->Connect();

$query = 'SET CHARACTER SET utf8';
$mysqlProxy->ExecuteQuery($query);

$query = "SELECT `date`, `book`, `chapter`, `verse`, `color_index`, `operation_type` FROM verses_highlights "
    . "WHERE user_id=" . $decodedJson["userId"] . " "
    . "AND date>='" . $decodedJson["lastSynchronizationDate"] . "' "
    . "ORDER BY date;";

if ($trace == 1) {
    echo $query . "<br />";
}

$query_result = $mysqlProxy->ExecuteQuery($query);
$device_unknown_verses_highlights = $mysqlProxy->FetchAll($query_result);

// Ajouter les données
if (count($decodedJson["versesHighlights"]) > 0) {
    for ($i = 0; $i < count($decodedJson["versesHighlights"]); $i++) {
        $verse_highlight = $decodedJson["versesHighlights"][$i];

        $query = "SELECT id FROM verses_highlights WHERE "
            . "user_id=" . $decodedJson["userId"] . " AND "
            . "date='" . $verse_highlight["date"] . "' AND "
            . "book=" . $verse_highlight["book"] . " AND "
            . "chapter=" . $verse_highlight["chapter"] . " AND "
            . "verse=" . $verse_highlight["verse"] . " AND "
            . "color_index=" . $verse_highlight["colorIndex"] . " AND "
            . "operation_type='" . $verse_highlight["operationType"] . "';";

        if ($trace == 1) {
            echo $query . "<br />";
        }

        $query_result = $mysqlProxy->ExecuteQuery($query);
        $query_assoc = $mysqlProxy->FetchArray($query_result);

        if (empty($query_assoc["id"])) {
            $query = "INSERT INTO verses_highlights (user_id,date,book,chapter,verse,color_index,operation_type) VALUES ("
                . $decodedJson["userId"] . ","
                . "'" . $verse_highlight["date"] . "',"
                . $verse_highlight["book"] . ","
                . $verse_highlight["chapter"] . ","
                . $verse_highlight["verse"] . ","
                . $verse_highlight["colorIndex"] . ","
                . "'" . $verse_highlight["operationType"] . "'"
                . ");";

            if ($trace == 1) {
                echo $query . "<br />";
            }

            $query_result = $mysqlProxy->ExecuteQuery($query);
        }
    }
}

$mysqlProxy->Disconnect();

// Envoyer les données
if (count($device_unknown_verses_highlights) > 0) {
    $flags = JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_UNESCAPED_SLASHES;

    $result = json_encode($device_unknown_verses_highlights, $flags);
    if ($trace == 1) {
        require_once "jsonHelper.php";
        $result = prettyPrint($result);
    }

    if ($trace == 1) {
        echo "<pre>";
        print_r($result);
        echo "</pre>";
    } else {
        echo $result;
    }
}
