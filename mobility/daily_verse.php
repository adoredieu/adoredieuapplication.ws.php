<?php
header('Content-Type: text/html; charset=UTF-8');

$trace = $_GET["trace"];

if ($trace != 1)
{
    // Désactiver le rapport d'erreurs
    error_reporting(0);
}

require_once "const.php";
require_once("PHP_DatabaseLib/MysqlProxyPhp7.php");

$mysqlProxy = new MysqlProxy(DATABASE_SERVER, DATABASE_USER, DATABASE_PASSWORD, MOBILITY_DATABASE_NAME);
$mysqlProxy->Connect();

$query = 'SET CHARACTER SET utf8';
$mysqlProxy->ExecuteQuery($query);

$dayNumber = 1;
$query = "SELECT `verse` "
    . "FROM daily_verses "
    . "WHERE id=DAYOFYEAR(CURDATE())";

if ($trace == 1)
{
    echo $query . "<br />";
}

$query_result = $mysqlProxy->ExecuteQuery($query);

$data = $mysqlProxy->FetchAssoc($query_result);

$mysqlProxy->Disconnect();

echo $data["verse"];
?>
