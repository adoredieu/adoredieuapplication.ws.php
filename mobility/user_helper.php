<?php
require_once WORDPRESS_DIR . "/wp-load.php";
require_once "const.php";
require_once("PHP_DatabaseLib/MysqlProxyPhp7.php");

function loadUser($login, $trace)
{
    $user = get_user_by('login', $login);

    $mysqlProxy = new MysqlProxy(DATABASE_SERVER, DATABASE_USER, DATABASE_PASSWORD, CONTENT_DATABASE_NAME);
    $mysqlProxy->Connect();

    $query = 'SET CHARACTER SET utf8';
    $mysqlProxy->ExecuteQuery($query);

    $query = "SELECT meta_value FROM " . DATABASE_PREFIX . "usermeta WHERE user_id='" . $user->ID . "' AND meta_key='user_pic'";
    if ($trace == 1) {
        echo $query . "<br />";
    }

    $query_result = $mysqlProxy->ExecuteQuery($query);
    $user_meta = $mysqlProxy->FetchAssoc($query_result);

    $result["success"] = 1;
    $result["id"] = $user->ID;
    $result["login"] = $user->data->user_login;
    $result["display_name"] = $user->data->display_name;
    $result["user_email"] = $user->data->user_email;

    $result["picture"] = "";
    if ($user_meta["meta_value"] != null) {
        // http://www.adoredieu.com/adwp/wp-content/uploads/usersultramedia/5747/avatar_idrooz4_eb18e7ffa5f8e70a9a743465d8bc273e_1491830538.png
        $result["picture"] = DOMAIN_NAME . "/wp-content/uploads/usersultramedia/" . $user->ID . "/" . $user_meta["meta_value"];
    }

    $mysqlProxy->Disconnect();

    return $result;
}

?>