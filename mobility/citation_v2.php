<?php
require_once "cors.php";
cors();
header('Content-Type: text/html; charset=UTF-8');

$trace = $_GET["trace"];
$day = $_GET["day"];

if ($trace != 1) {
    // Désactiver le rapport d'erreurs
    error_reporting(0);
}

require_once "const.php";
require_once("PHP_DatabaseLib/MysqlProxyPhp7.php");

$mysqlProxy = new MysqlProxy(DATABASE_SERVER, DATABASE_USER, DATABASE_PASSWORD, CONTENT_DATABASE_NAME);
$mysqlProxy->Connect();

$query = 'SET CHARACTER SET utf8';
$mysqlProxy->ExecuteQuery($query);

$query = "SELECT `post_title` "
    . "FROM " . DATABASE_PREFIX . "posts "
    . "WHERE post_type='" . CITATIONS_CAT_ID . "' AND post_date<='" . $day . "' "
    . "AND post_title!='Brouillon auto' "
    . "ORDER BY post_date DESC LIMIT 1";

if ($trace == 1) {
    echo $query . "<br />";
}

$query_result = $mysqlProxy->ExecuteQuery($query);

$data = $mysqlProxy->FetchAssoc($query_result);

$mysqlProxy->Disconnect();

echo trim(strip_tags($data["post_title"]));
?>
