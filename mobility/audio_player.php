<?php
require_once "cors.php";
cors();
header('Content-Type: text/html; charset=UTF-8');
$trace = $_GET["trace"];

if ($trace != 1) {
    // Désactiver le rapport d'erreurs
    error_reporting(0);
}

require_once "const.php";
require_once("PHP_DatabaseLib/MysqlProxyPhp7.php");
require_once "strings_helper.php";

$mysqlProxy = new MysqlProxy(DATABASE_SERVER, DATABASE_USER, DATABASE_PASSWORD, MOBILITY_DATABASE_NAME);
$mysqlProxy->Connect();

$query = 'SET CHARACTER SET utf8';
$mysqlProxy->ExecuteQuery($query);

$query = "SELECT * FROM audio_playlist";

if ($trace == 1) {
    echo $query . "<br />";
}

$query_result = $mysqlProxy->ExecuteQuery($query);

$audios = $mysqlProxy->FetchAll($query_result);

$mysqlProxy->Disconnect();
if ($trace == 1) {
    echo "<pre>";
}

$tracks = null;
$i = 0;
foreach ($audios as $audio) {
    $tracks[$i]["artist"] = $audio["artist"];
    $tracks[$i]["title"] = $audio["title"];

    if ($audio["cover"] == null
        || endsWith($audio["cover"], "playlistfemme.jpg")
        || endsWith($audio["cover"], "playlisthomme.jpg")
    ) {
        $tracks[$i]["cover"] = "";
    } else {
        $tracks[$i]["cover"] = str_replace(" ", "%20", $audio["cover"]);
    }

    $tracks[$i]["file"] = str_replace(" ", "%20", $audio["file"]);
    $i++;
}

$flags = JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_UNESCAPED_SLASHES;

$tracksJsonContent = json_encode($tracks, $flags);

if ($trace == 1) {
    require_once "jsonHelper.php";
    $tracksJsonContent = prettyPrint($tracksJsonContent);
    print_r($tracksJsonContent);
    echo "</pre>";
} else {
    echo $tracksJsonContent;
}
?>
