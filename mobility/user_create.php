<?php
require_once "const.php";
require_once "cors.php";
cors();
header('Content-Type: text/html; charset=UTF-8');

$login = htmlspecialchars($_GET["login"]);
$password = htmlspecialchars($_GET["password"]);
$displayName = htmlspecialchars($_GET["displayName"]);
$email = htmlspecialchars($_GET["email"]);
$trace = $_GET["trace"];

if ($trace != 1) {
    // Désactiver le rapport d'erreurs
    error_reporting(0);
}

require_once WORDPRESS_DIR . "/wp-load.php";

$userdata = array(
    'user_login' => $login,
    'user_pass' => $password,
    'display_name' => $displayName,
    'user_email' => $email
);

$user_id = wp_insert_user($userdata);

// On success.
if (!is_wp_error($user_id)) {
    require_once "user_helper.php";
    $userInfo = loadUser($login, $trace);
} else {
    $userInfo["success"] = 0;

    if ($trace) {
        $userInfo['error'] = $user_id;
    }
}

require_once "http_helper.php";
sendJsonResponse($userInfo, $trace);
?>
