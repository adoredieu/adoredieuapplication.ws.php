<?php
require_once "cors.php";
cors();
date_default_timezone_set('Europe/Paris');
header('Content-Type: text/html; charset=UTF-8');

$request = urldecode($_GET["request"]);
$request_terms = explode(" ", $request);

$trace = $_GET["trace"];

if ($trace != 1) {
    // Désactiver le rapport d'erreurs
    error_reporting(0);
}

require_once "const.php";
require_once "strings_helper.php";
require_once("PHP_DatabaseLib/MysqlProxyPhp7.php");

$mysqlProxy = new MysqlProxy(DATABASE_SERVER, DATABASE_USER, DATABASE_PASSWORD, CONTENT_DATABASE_NAME);
$mysqlProxy->Connect();

$query = 'SET CHARACTER SET utf8';
$mysqlProxy->ExecuteQuery($query);

$result = "";
$item_per_page = MAX_SEARCH_RESULTS;

$andQuery = "";
foreach ($request_terms as $term) {
    $term = $mysqlProxy->EscapeString($term);
    $andQuery .= "AND ((`post_title` LIKE '%$term%') OR (`post_content` LIKE '%$term%')) ";
}

$query = "SELECT `id`, `post_title`, `post_date`, `post_name`, `post_content` "
    . "FROM " . DATABASE_PREFIX . "posts "
    . "WHERE post_type='" . PENSEES_CAT_ID . "' AND post_date<='" . date("c") . "' "
    . $andQuery . " "
    . "ORDER BY post_date DESC LIMIT 0, " . $item_per_page . "; ";

if ($trace == 1) {
    echo $query . "<br />";
}

$query_result = $mysqlProxy->ExecuteQuery($query);

$articles = $mysqlProxy->FetchAll($query_result);

$articlesFormatted = null;
$i = 0;
foreach ($articles as $article) {
    $articlesFormatted[$i]["id"] = $article["id"];

    $articlesFormatted[$i]["publish_up"] = $article["post_date"];

    $articlesFormatted[$i]["article_link"] = PENSEES_BASE_LINK . $article["post_name"] . '/';

    $articlesFormatted[$i]["title"] = trim(strip_tags($article["post_title"]));

    $articlesFormatted[$i]["introtext"] = cutStringOnWords(strip_tags($article["post_content"]), 260) . "...";

    $articlesFormatted[$i]["thumbnail"] = "";

    $i++;
}

$flags = JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_UNESCAPED_SLASHES;

$result = json_encode($articlesFormatted, $flags);
if ($trace == 1) {
    require_once "jsonHelper.php";
    $result = prettyPrint($result);
}

$mysqlProxy->Disconnect();

if ($trace == 1) {
    echo "<pre>";
    print_r($result);
    echo "</pre>";
} else {
    echo $result;
}

?>
