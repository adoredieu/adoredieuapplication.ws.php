<?php
require_once "cors.php";
cors();
header('Content-Type: text/html; charset=UTF-8');

$trace = $_GET["trace"];
$version = $_GET["version"];

$userId = $_GET["userId"];
$articleId = $_GET["articleId"];
$operationType = $_GET["operationType"];

if ($trace != 1) {
    // Désactiver le rapport d'erreurs
    error_reporting(0);
}

require_once "const.php";
require_once("PHP_DatabaseLib/MysqlProxyPhp7.php");

$mysqlProxy = new MysqlProxy(DATABASE_SERVER, DATABASE_USER, DATABASE_PASSWORD, MOBILITY_DATABASE_NAME);
$mysqlProxy->Connect();

$query = 'SET CHARACTER SET utf8';
$mysqlProxy->ExecuteQuery($query);

switch ($operationType) {
    case "ADD":
        $query = "INSERT INTO articles_favourites (user_id,article_id) VALUES ($userId, $articleId);";
        $query_result = $mysqlProxy->ExecuteQuery($query);
        echo $query_result;
        break;
    case "DELETE":
        $query = "DELETE FROM articles_favourites WHERE user_id=$userId AND article_id=$articleId;";
        $query_result = $mysqlProxy->ExecuteQuery($query);
        echo $query_result;
        break;
    case "LIST":
        $query = "SELECT article_id FROM articles_favourites WHERE user_id=$userId;";
        $query_result = $mysqlProxy->ExecuteQuery($query);

        $favourites = $mysqlProxy->FetchAll($query_result);
        $favouritesFormated = null;
        $i = 0;
        foreach ($favourites as $favourite)
        {
            $favouritesFormated[$i] = $favourite["article_id"];
            $i++;
        }

        $flags = JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_UNESCAPED_SLASHES;

        $result = json_encode($favouritesFormated, $flags);
        if ($trace == 1)
        {
            require_once "jsonHelper.php";
            $result = prettyPrint($result);
        }

        if ($trace == 1)
        {
            echo "<pre>";
            print_r($result);
            echo "</pre>";
        }
        else
        {
            echo $result;
        }
        break;
}

$mysqlProxy->Disconnect();
