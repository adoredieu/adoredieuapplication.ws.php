<?php
function getVideoLink($mysqlProxy, $id)
{
    $query = "SELECT `meta_value` FROM " . DATABASE_PREFIX . "postmeta WHERE post_id=" . $id . " AND meta_key = 'url_video'";

    $query_result = $mysqlProxy->ExecuteQuery($query);
    $metaData = $mysqlProxy->FetchAssoc($query_result);

    return $metaData;
}

function getThumbnail($mysqlProxy, $id)
{
    $query = "SELECT `meta_value` FROM " . DATABASE_PREFIX . "postmeta 
        WHERE post_id=(SELECT `meta_value` FROM " . DATABASE_PREFIX . "postmeta WHERE post_id=" . $id . " AND meta_key = '_thumbnail_id') 
        AND meta_key='_wp_attached_file';";

    $query_result = $mysqlProxy->ExecuteQuery($query);
    $metaData = $mysqlProxy->FetchAssoc($query_result);

    return $metaData;
}

require_once "cors.php";
cors();
date_default_timezone_set('Europe/Paris');
header('Content-Type: text/html; charset=UTF-8');

$getLastDate = $_GET["getLastDate"];
$page = $_GET["page"];
$item_per_page = $_GET["items_per_page"];
$trace = $_GET["trace"];

if ($trace != 1) {
    // Désactiver le rapport d'erreurs
    error_reporting(0);
}

require_once "const.php";
require_once "strings_helper.php";
require_once("PHP_DatabaseLib/MysqlProxyPhp7.php");

$mysqlProxy = new MysqlProxy(DATABASE_SERVER, DATABASE_USER, DATABASE_PASSWORD, CONTENT_DATABASE_NAME);
$mysqlProxy->Connect();

$query = 'SET CHARACTER SET utf8';
$mysqlProxy->ExecuteQuery($query);

$result = "";

if ($getLastDate == 1) {
    $query = "SELECT `post_date` "
        . "FROM " . DATABASE_PREFIX . "posts "
        . "WHERE post_type='" . ENSEIGNEMENTS_CAT_ID . " AND post_date<='" . date("c") . "' "
        . "AND post_title!='Brouillon auto' "
        . "ORDER BY post_date DESC LIMIT 1";

    if ($trace == 1) {
        echo $query . "<br />";
    }

    $query_result = $mysqlProxy->ExecuteQuery($query);
    $lastArticle = $mysqlProxy->FetchAssoc($query_result);
    $result = $lastArticle["post_date"];
} else {
    if (empty($page)) {
        $page = 0;
    }
    if (empty($item_per_page)) {
        $item_per_page = 5;
    }

    $start_from = $page * $item_per_page;
    $query = "SELECT `id`, `post_title`, `post_date`, `post_name`, `post_content` "
        . "FROM " . DATABASE_PREFIX . "posts "
        . "WHERE post_type='" . ENSEIGNEMENTS_CAT_ID . "' "
        . "AND post_date<='" . date("c") . "' "
        . "AND post_title!='Brouillon auto' "
        . "ORDER BY post_date DESC LIMIT " . $start_from . ", " . $item_per_page . "; ";

    if ($trace == 1) {
        echo $query . "<br />";
    }

    $query_result = $mysqlProxy->ExecuteQuery($query);

    $articles = $mysqlProxy->FetchAll($query_result);

    $articlesFormatted = null;
    $i = 0;
    foreach ($articles as $article) {
        $articlesFormatted[$i]["id"] = $article["id"];

        $articlesFormatted[$i]["publish_up"] = $article["post_date"];

        $articlesFormatted[$i]["article_link"] = ENSEIGNEMENTS_BASE_LINK . $article["post_name"] . '/';

        $trimmedTitle = trim(strip_tags($article["post_title"]));
        $articlesFormatted[$i]["title"] = $trimmedTitle;
        $articlesFormatted[$i]["introtext"] = trim(cutStringOnWords(strip_tags($article["post_content"]), 260) . "...");

        $youtube_video_id = "";
        $metaData = getVideoLink($mysqlProxy, $article['id']);
        if (strlen($metaData["meta_value"]) != 0) {
            // https://www.youtube.com/watch?v=bXhzmVbGqC0
            $youtube_video_id = substr($metaData["meta_value"], strlen("https://www.youtube.com/watch?v="));
        }

        $articlesFormatted[$i]["videoId"] = $youtube_video_id;

        $thumbnail = "";
        $metaData = getThumbnail($mysqlProxy, $article['id']);
        if (strlen($metaData["meta_value"]) != 0) {
            $thumbnail = DOMAIN_NAME . "/wp-content/uploads/" . $metaData["meta_value"];
        }

        $articlesFormatted[$i]["thumbnail"] = $thumbnail;

        $i++;
    }

    $flags = JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_UNESCAPED_SLASHES;

    $result = json_encode($articlesFormatted, $flags);
    if ($trace == 1) {
        require_once "jsonHelper.php";
        $result = prettyPrint($result);
    }
}

$mysqlProxy->Disconnect();

if ($trace == 1) {
    echo "<pre>";
    print_r($result);
    echo "</pre>";
} else {
    echo $result;
}

?>
