<?php
require_once "cors.php";
cors();
header('Content-Type: text/html; charset=UTF-8');

$id = $_GET["id"];
$trace = $_GET["trace"];

if ($trace != 1) {
    // Désactiver le rapport d'erreurs
    error_reporting(0);
}

require_once "const.php";
require_once("PHP_DatabaseLib/MysqlProxyPhp7.php");
require_once "strings_helper.php";

// Stats
$mysqlProxy = new MysqlProxy(DATABASE_SERVER, DATABASE_USER, DATABASE_PASSWORD, MOBILITY_DATABASE_NAME);
$mysqlProxy->Connect();

$query = "SELECT `id` FROM articles_stats WHERE article_id=" . $id;
if ($trace == 1) {
    echo $query . "<br />";
}

$query_result = $mysqlProxy->ExecuteQuery($query);
if (mysqli_fetch_array($query_result) !== false) {
    $query = "UPDATE articles_stats SET view_count = view_count + 1 WHERE article_id=" . $id;
} else {
    $query = "INSERT INTO articles_stats (article_id, view_count) VALUES($id, 1)";
}
$mysqlProxy->ExecuteQuery($query);

$mysqlProxy->Disconnect();

// Article
$mysqlProxy = new MysqlProxy(DATABASE_SERVER, DATABASE_USER, DATABASE_PASSWORD, CONTENT_DATABASE_NAME);
$mysqlProxy->Connect();

$query = 'SET CHARACTER SET utf8';
$mysqlProxy->ExecuteQuery($query);

$result = "";
$query = "SELECT `id`, `post_title`, `post_date`, `post_name`, `post_content` "
    . "FROM " . DATABASE_PREFIX . "posts WHERE id=" . $id;

if ($trace == 1) {
    echo $query . "<br />";
}

$query_result = $mysqlProxy->ExecuteQuery($query);

$article = $mysqlProxy->FetchAssoc($query_result);

$query = "SELECT `meta_value` FROM " . DATABASE_PREFIX . "postmeta WHERE post_id=" . $article["id"] . " AND meta_key = 'url_video'";

$query_result = $mysqlProxy->ExecuteQuery($query);
$metaData = $mysqlProxy->FetchAssoc($query_result);

if ($trace == 1) {
    echo $query . "<br />";
}

$content = $article["post_content"];
$content = cleanArticlePostContent($content);

if (strlen($metaData["meta_value"]) != 0) {
    // https://www.youtube.com/watch?v=bXhzmVbGqC0
    // <iframe width="854" height="480" src="https://www.youtube.com/embed/bXhzmVbGqC0" frameborder="0" allowfullscreen></iframe>
    $youtube_video_id = substr($metaData["meta_value"], strlen("https://www.youtube.com/watch?v="));
    $content = $content . '<iframe width="400" height="200" src="https://www.youtube.com/embed/' . $youtube_video_id . '" frameborder="0" allowfullscreen></iframe>';
}

$articleFormatted["content"] = $content;

$article_fb_link = PENSEES_BASE_LINK . $article["post_name"] . '/';
$articleFormatted["fb_article_link"] = $article_fb_link;

$trimmedTitle = trim(strip_tags($article["post_title"]));
$articleFormatted["title"] = $trimmedTitle;

$articleFormatted["publish_up"] = $article["post_date"];

$flags = JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_UNESCAPED_SLASHES;

$result = json_encode($articleFormatted, $flags);
if ($trace == 1) {
    require_once "jsonHelper.php";
    $result = prettyPrint($result);
}

$mysqlProxy->Disconnect();

if ($trace == 1) {
    echo "<pre>";
    print_r($result);
    echo "</pre>";
} else {
    echo $result;
}

?>
