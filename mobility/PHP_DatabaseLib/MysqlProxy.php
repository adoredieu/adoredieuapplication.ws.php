﻿<?php

/**
 * Description of MysqlProxy
 *
 * @author Zainal
 */
class MysqlProxy
{
    private $_host = "";
    private $_login = "";
    private $_password = "";
    private $_database = "";
    private $_dbHandle = "";
    private $_connected = false;

    public function __construct($host, $login, $password, $database)
    {
        $this->_host = $host;
        $this->_login = $login;
        $this->_password = $password;
        $this->_database = $database;
        $this->_dbHandle = null;
    }

    public function __destruct()
    {
        if ($this->IsConnected())
        {
            $this->Disconnect();
        }
    }

    public function IsConnected()
    {
        return $this->_connected;
    }

    public function Disconnect()
    {
        if ($this->IsConnected())
        {
            $this->_connected = !mysql_close($this->_dbHandle);
        }
        else
        {
            throw new Exception("Non connecté.");
        }
    }

    public function Connect()
    {
        if (!$this->IsConnected())
        {
            $dbHandle = mysql_connect($this->_host, $this->_login, $this->_password);
            $ok = null;

            if (!$dbHandle)
            {
                throw new Exception("Connection impossible au serveur Mysql " . $this->_host . ' (' . mysql_error() . ')');
            }
            else
            {
                $ok = mysql_select_db($this->_database, $dbHandle);
            }

            if (!$ok)
            {
                throw new Exception("Connection impossible à la base " . $this->_database. ' (' . mysql_error() . ')');
            }

            $this->_dbHandle = $dbHandle;
            $this->_connected = true;
        }
    }

    public function DEBUG_PrintResult($queryResult)
    {
        /* quelle est la dimension du résultat ? */
        $nblignes = $this->FetchNumRows($queryResult);
        $nbchamps = $this->FetchNumFields($queryResult);
        $tabchamps = null;

        /* affichage de l'en-tete du tableau HTML avec les noms des champs */
        echo "<table border=2 align=center><tr>";
        for ($i = 0; $i < $nbchamps; $i++)
        {
            $tabchamps[$i] = $this->FetchFieldName($queryResult, $i);
            echo "<th>$tabchamps[$i]</th>";
        }
        echo "</tr>";

        /* affichage des enregistrements du résultat */
        for ($i = 0; $i < $nblignes; $i++)
        {
            $ligne = $this->FetchRow($queryResult);
            echo "<tr>";
            for ($j = 0; $j < $nbchamps; $j++)
            {
                echo "<td>$ligne[$j]</td>";
            }
            echo "</tr>";
        }
        echo "</table>";
    }

    public function FetchNumRows($queryResult)
    {
        if ($this->IsConnected())
        {
            return mysql_num_rows($queryResult);
        }
        else
        {
            throw new Exception("Non connecté.");
        }
    }

    public function FetchNumFields($queryResult)
    {
        if ($this->IsConnected())
        {
            return mysql_num_fields($queryResult);
        }
        else
        {
            throw new Exception("Non connecté.");
        }
    }

    public function FetchFieldName($queryResult)
    {
        if ($this->IsConnected())
        {
            return mysql_field_name($queryResult);
        }
        else
        {
            throw new Exception("Non connecté.");
        }
    }

    public function FetchRow($queryResult)
    {
        if ($this->IsConnected())
        {
            return mysql_fetch_row($queryResult);
        }
        else
        {
            throw new Exception("Non connecté.");
        }
    }

    public function FetchAssoc($queryResult)
    {
        if ($this->IsConnected())
        {
            return mysql_fetch_assoc($queryResult);
        }
        else
        {
            throw new Exception("Non connecté.");
        }
    }

    public function FetchAll($queryResult)
    {
        if ($this->IsConnected())
        {
            $result = null;
            $i = 0;

            while ($row = mysql_fetch_assoc($queryResult))
            {
                $result[count($result)] = $row;
                $i++;
            }
            return $result;
        }
        else
        {
            throw new Exception("Non connecté.");
        }
    }

    public function LastInsertId()
    {
        return mysql_insert_id();
    }

    public function DumpDatabase($mode, $newLineString)
    {
        if ($this->IsConnected())
        {
            $entete = "-- ----------------------" . $newLineString;
            $entete .= "-- dump de la base " . $this->_database . " au " . date("d-M-Y") . $newLineString;
            $entete .= "-- ----------------------" . $newLineString . $newLineString . $newLineString;
            $creations = "";
            $insertions = $newLineString . $newLineString;

            $listeTables = $this->ExecuteQuery("show tables");
            while ($table = $this->FetchArray($listeTables))
            {
                if (substr($table[0], 0, 3) != "ps_")
                {
                    // si l'utilisateur a demandé la structure ou la totale
                    if ($mode == 1 || $mode == 3)
                    {
                        $creations .= "-- -----------------------------" . $newLineString;
                        $creations .= "-- creation de la table " . $table[0] . $newLineString;
                        $creations .= "-- -----------------------------" . $newLineString;
                        $creations .= "DROP TABLE IF EXISTS $table[0];" . $newLineString;
                        $listeCreationsTables = $this->ExecuteQuery("show create table " . $table[0]);
                        while ($creationTable = $this->FetchArray($listeCreationsTables))
                        {
                            $creations .= $creationTable[1] . ";" . $newLineString . $newLineString;
                        }
                    }
                    // si l'utilisateur a demandé les données ou la totale
                    if ($mode > 1)
                    {
                        $donnees = $this->ExecuteQuery("SELECT * FROM " . $table[0]);
                        $insertions .= "-- -----------------------------" . $newLineString;
                        $insertions .= "-- insertions dans la table " . $table[0] . $newLineString;
                        $insertions .= "-- -----------------------------" . $newLineString;
                        while ($nuplet = $this->FetchArray($donnees))
                        {
                            $insertions .= "INSERT INTO " . $table[0] . " VALUES(";
                            for ($i = 0; $i < mysql_num_fields($donnees); $i++)
                            {
                                if ($i != 0)
                                {
                                    $insertions .= ", ";
                                }
                                if (mysql_field_type($donnees, $i) != "int"
                                    && mysql_field_type($donnees, $i) != "real"
                                )
                                {
                                    $insertions .= "'";
                                }
                                $insertions .= addslashes($nuplet[$i]);
                                if (mysql_field_type($donnees, $i) != "int"
                                    && mysql_field_type($donnees, $i) != "real"
                                )
                                {
                                    $insertions .= "'";
                                }
                            }
                            $insertions .= ");" . $newLineString;
                        }
                        $insertions .= $newLineString;
                    }
                }
            }

            $result = $entete;
            $result .= $creations;
            $result .= $insertions;

            return $result;
        }
        else
        {
            throw new Exception("Non connecté.");
        }
    }

    public function ExecuteQuery($query)
    {
        if ($this->IsConnected())
        {
            /* query de sélection */
            $result = mysql_query($query, $this->_dbHandle);
            if ($result)
            {
                return $result;
            }
            else
            {
                throw new Exception("Erreur dans la requête '" . $query . "'; Message de MySql : '" . mysql_errno($this->_dbHandle) . "' " . mysql_error($this->_dbHandle));
            }
        }
        else
        {
            throw new Exception("Non connecté.");
        }
    }

    public function FetchArray($queryResult)
    {
        if ($this->IsConnected())
        {
            return mysql_fetch_array($queryResult);
        }
        else
        {
            throw new Exception("Non connecté.");
        }
    }
}

?>
