﻿<?php

/**
 * Description of MysqlProxy
 *
 * @author Zainal
 */
class MysqlProxy
{
    private $_host = "";
    private $_login = "";
    private $_password = "";
    private $_database = "";
    private $_mysqli = "";
    private $_connected = false;

    public function __construct($host, $login, $password, $database)
    {
        $this->_host = $host;
        $this->_login = $login;
        $this->_password = $password;
        $this->_database = $database;
        $this->_mysqli = null;
    }

    public function __destruct()
    {
        if ($this->IsConnected()) {
            $this->Disconnect();
        }
    }

    public function IsConnected()
    {
        return $this->_connected;
    }

    public function Disconnect()
    {
        if ($this->IsConnected()) {
            $this->_connected = !mysqli_close($this->_mysqli);
        } else {
            throw new Exception("Non connecté.");
        }
    }

    public function Connect()
    {
        if (!$this->IsConnected()) {
            $mysqli = mysqli_connect($this->_host, $this->_login, $this->_password, $this->_database);

            /* Vérification de la connexion */
            if (mysqli_connect_errno()) {
                throw new Exception("Échec de la connexion : %s\n", mysqli_connect_error());
            }

            /* Modification du jeu de résultats en utf8 */
            if (!$mysqli->set_charset("utf8")) {
                throw new Exception("Erreur lors du chargement du jeu de caractères utf8 : %s\n", $mysqli->error);
            }

            $this->_mysqli = $mysqli;
            $this->_connected = true;
        }
    }

    public function FetchNumRows($queryResult)
    {
        if ($this->IsConnected()) {
            return mysqli_num_rows($queryResult);
        } else {
            throw new Exception("Non connecté.");
        }
    }

    public function FetchNumFields($queryResult)
    {
        if ($this->IsConnected()) {
            return mysqli_num_fields($queryResult);
        } else {
            throw new Exception("Non connecté.");
        }
    }

    public function FetchRow($queryResult)
    {
        if ($this->IsConnected()) {
            return mysqli_fetch_row($queryResult);
        } else {
            throw new Exception("Non connecté.");
        }
    }

    public function FetchAssoc($queryResult)
    {
        if ($this->IsConnected()) {
            return mysqli_fetch_assoc($queryResult);
        } else {
            throw new Exception("Non connecté.");
        }
    }

    public function FetchAll($queryResult)
    {
        if ($this->IsConnected()) {
            $result = null;
            $i = 0;

            while ($row = mysqli_fetch_assoc($queryResult)) {
                $result[count($result)] = $row;
                $i++;
            }
            return $result;
        } else {
            throw new Exception("Non connecté.");
        }
    }

    public function LastInsertId()
    {
        return mysqli_insert_id($this->_mysqli);
    }

    public function ExecuteQuery($query)
    {
        if ($query == "SET CHARACTER SET utf8")
            return true;

        if ($this->IsConnected()) {
            /* query de sélection */
            $result = mysqli_query($this->_mysqli, $query);

            if ($result != FALSE) {
                return $result;
            } else {
                throw new Exception("Erreur dans la requête '" . $query . "'; Message de MySql : '" . mysqli_errno($this->_mysqli) . "' " . mysqli_error($this->_mysqli));
            }
        } else {
            throw new Exception("Non connecté.");
        }
    }

    public function FetchArray($queryResult)
    {
        if ($this->IsConnected()) {
            return mysqli_fetch_array($queryResult);
        } else {
            throw new Exception("Non connecté.");
        }
    }

    public function EscapeString($string)
    {
        if ($this->IsConnected()) {
            return mysqli_real_escape_string ($this->_mysqli, $string);
        } else {
            throw new Exception("Non connecté.");
        }
    }
}

?>
