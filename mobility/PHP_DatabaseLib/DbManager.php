﻿<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DbManager
 *
 * @author Zainal
 */
abstract class DbManager
{
   public function SafeStringEscape($str)
   {
      $len = strlen($str);
      $escapeCount = 0;
      $targetString = '';
      for ($offset = 0; $offset < $len; $offset++)
      {
         switch ($c = $str{$offset})
         {
            case "'":
               // Escapes this quote only if its not preceded by an unescaped backslash
               if ($escapeCount % 2 == 0)
                  $targetString.="\\";
               $escapeCount = 0;
               $targetString.=$c;
               break;
            case '"':
               // Escapes this quote only if its not preceded by an unescaped backslash
               if ($escapeCount % 2 == 0)
                  $targetString.="\\";
               $escapeCount = 0;
               $targetString.=$c;
               break;
            case '\\':
               $escapeCount++;
               $targetString.=$c;
               break;
            default:
               $escapeCount = 0;
               $targetString.=$c;
         }
      }
      return $targetString;
   }

   function EscapeAndHtmlEncodeString($StringToEncode)
   {
      return $this->SafeStringEscape(htmlentities(trim($StringToEncode)));
   }

   function UnescapeAndHtmlDecodeString($StringToDecode)
   {
      return stripslashes(html_entity_decode(trim($StringToDecode)));
   }
}
?>
