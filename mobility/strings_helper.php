<?php

function startsWith($haystack, $needle)
{
    return $needle === "" || strpos($haystack, $needle) === 0;
}

function endsWith($haystack, $needle)
{
    return $needle === "" || substr($haystack, -strlen($needle)) === $needle;
}

function cutStringOnWords($string, $length)
{
    $line = $string;
    if (preg_match('/^.{1,' . $length . '}\b/s', $string, $match)) {
        $line = $match[0];
    }
    return $line;
}

function cleanArticlePostContent($content)
{
    $content = str_ireplace("{loadposition face}", "", $content);
    $content = preg_replace('/<img[^>]*?>/', "", $content);

// Enlever tout les paragraphes vides du contenu
    $content = preg_replace('#<p>\s*</p>#', '', $content);
    $content = preg_replace('#<p>&nbsp;</p>#', '', $content);
    $content = preg_replace('#<p>\xc2\xa0</p>#', '', $content);
    $content = preg_replace('#</*blockquote>#', '', $content);

    // Enlever tout les paragraphes vides en fin de contenu
    while (endsWith($content, "<br />")) {
        $content = substr($content, 0, strlen($content) - strlen("<br />"));
    }

    return $content;
}

?>