<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN\"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <script type="application/javascript" src="facebook_sdk.js"></script>
</head>
<body>
<div id="fb-root"></div>
<?php
// Désactiver le rapport d'erreurs
error_reporting(0);

require_once "const.php";
require_once("PHP_DatabaseLib/MysqlProxyPhp7.php");
require_once "strings_helper.php";

$mysqlProxy = new MysqlProxy(DATABASE_SERVER, DATABASE_USER, DATABASE_PASSWORD, CONTENT_DATABASE_NAME);
$mysqlProxy->Connect();

$query = 'SET CHARACTER SET utf8';
$mysqlProxy->ExecuteQuery($query);

$query = "SELECT `id`, `post_title`, `post_date`, `post_name`, `post_content` "
    . "FROM " . DATABASE_PREFIX . "posts WHERE id=" . $_GET["id"];

$query_result = $mysqlProxy->ExecuteQuery($query);

$article = $mysqlProxy->FetchAssoc($query_result);

$content = $article["post_content"];
$content = cleanArticlePostContent($content);

$article_link = ENSEIGNEMENTS_BASE_LINK . $article["post_name"] . '/';

$mysqlProxy->Disconnect();

echo $content;
?>
<div class="fb-like" data-href="<?php echo $article_link; ?>" data-layout="button_count"
     data-action="like"
     data-show-faces="true" data-share="true"></div>
</body>
</html>
