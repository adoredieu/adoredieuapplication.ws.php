<html>
<head>
    <title>Statistiques sur les articles lus</title>
    <script src="js/sorttable.js"></script>
    <style type="text/css">
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
        th, td {
            padding: 15px;
        }
        /* Sortable tables */
        table.sortable thead {
            background-color:#eee;
            color:#666666;
            font-weight: bold;
            cursor: default;
        }
    </style>
</head>
<body>
    <h1>Statistiques sur les articles lus</h1>
    <table class="sortable">
    <tr>
        <th>Date de publication</th>
        <th>Titre</th>
        <th>Nombre de vues</th>
    </tr>
<?php
$trace = $_GET["trace"];

if ($trace != 1)
{
    // Désactiver le rapport d'erreurs
    error_reporting(0);
}

if ($trace == 1)
{
    echo "<pre>";
}

require_once "const.php";
require_once("PHP_DatabaseLib/MysqlProxyPhp7.php");

// Stats
$mysqlProxy = new MysqlProxy(DATABASE_SERVER, DATABASE_USER, DATABASE_PASSWORD, MOBILITY_DATABASE_NAME);
$mysqlProxy->Connect();

$query = "SELECT * FROM articles_stats ORDER BY 'publish_up';";
if ($trace == 1)
{
    echo $query . "<br />";
}

$query_result = $mysqlProxy->ExecuteQuery($query);
$articles_stats = $mysqlProxy->FetchAll($query_result);
$mysqlProxy->Disconnect();

$mysqlProxy = new MysqlProxy(DATABASE_SERVER, DATABASE_USER, DATABASE_PASSWORD, CONTENT_DATABASE_NAME);
$mysqlProxy->Connect();

$query = 'SET CHARACTER SET utf8';
$mysqlProxy->ExecuteQuery($query);

foreach ($articles_stats as $article_stat)
{
    $query = "SELECT `id`, `alias`, `title`, `publish_up`, `hits` "
        . "FROM " . DATABASE_PREFIX . "content WHERE id=" . $article_stat["article_id"];
    $query_result = $mysqlProxy->ExecuteQuery($query);
    $article = $mysqlProxy->FetchAssoc($query_result);

    echo "<tr>  <td>".$article["publish_up"]."</td>  <td>".$article["title"]."</td>  <td>".($article_stat["view_count"] + $article["hits"])."</td>  </tr>";
}

$mysqlProxy->Disconnect();

if ($trace == 1)
{
    echo "</pre>";
}
?>
</table>
</body>
</html>
