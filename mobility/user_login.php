<?php
require_once "const.php";
require_once "cors.php";
cors();
header('Content-Type: text/html; charset=UTF-8');

$username = htmlspecialchars($_GET["username"]);
$password = htmlspecialchars($_GET["pass"]);
$trace = $_GET["trace"];

if ($trace != 1) {
    // Désactiver le rapport d'erreurs
    error_reporting(0);
}

require_once WORDPRESS_DIR . "/wp-load.php";

$user = get_user_by('login', $username);

if ($trace == 1) {
    echo "<pre>";
    print_r($user);
    echo "</pre>";
}

if (!wp_check_password($password, $user->data->user_pass, $user->ID)) {
    $userInfo["success"] = 0;
} else {
    require_once "user_helper.php";
    $userInfo = loadUser($username, $trace);
}

require_once "http_helper.php";
sendJsonResponse($userInfo, $trace);
?>
