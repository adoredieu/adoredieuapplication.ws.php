<?php
function sendNotifications($registrations, $trace)
{
    $tokenCloudMessaging = "AAAALGJpYao:APA91bHbv7qyhahZRXojWX6J7mrkhBph7fyyKuVMwClTLvn7SsziLUlFECcLz6o6ZMe9JBetBj1JL61uIGMkrfvT2JHViFs5AqllUAOScse2c_ACtmVCpFqO0Gt9x0LFjueIFrUOAq42";

    // Call firebase
    $url = "https://fcm.googleapis.com/fcm/send";
    $curl = curl_init($url);

    $notificationBody = new stdClass();
    $notificationBody->registration_ids = array();
    $notificationBody->collapse_key = "Daily notification";

    $i = 0;
    foreach ($registrations as $registration) {
        $notificationBody->registration_ids[$i] = $registration['push_notification_token'];
        $i++;
    }

    $flags = JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_UNESCAPED_SLASHES;
    $curl_data = json_encode($notificationBody, $flags);

    if ($trace == 1) {
        echo "<pre>";
        print_r($curl_data);
        echo "</pre><br>";
    }

    $options = array(
        CURLOPT_RETURNTRANSFER => true,         // return web page
        CURLOPT_HEADER => false,        // don't return headers
        CURLOPT_FOLLOWLOCATION => true,         // follow redirects
        CURLOPT_ENCODING => "",           // handle all encodings
        CURLOPT_AUTOREFERER => true,         // set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 120,          // timeout on connect
        CURLOPT_TIMEOUT => 120,          // timeout on response
        CURLOPT_MAXREDIRS => 10,           // stop after 10 redirects
        CURLOPT_POST => 1,            // i am sending post data
        CURLOPT_POSTFIELDS => $curl_data,    // this are my post vars
        CURLOPT_SSL_VERIFYHOST => 0,            // don't verify ssl
        CURLOPT_SSL_VERIFYPEER => false,        //
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'Authorization: key=' . $tokenCloudMessaging
        ),
        CURLOPT_VERBOSE => 1,
    );

    curl_setopt_array($curl, $options);
    $result = curl_exec($curl);

    if ($trace == 1) {
        echo "<pre>";
        print_r($result);
        echo "</pre><br>";
    }
}

require_once "cors.php";
cors();
header('Content-Type: text/html; charset=UTF-8');

$trace = $_GET["trace"];

if ($trace != 1) {
    // Désactiver le rapport d'erreurs
    error_reporting(0);
}

require_once "const.php";
require_once("PHP_DatabaseLib/MysqlProxyPhp7.php");

$mysqlProxy = new MysqlProxy(DATABASE_SERVER, DATABASE_USER, DATABASE_PASSWORD, MOBILITY_DATABASE_NAME);
$mysqlProxy->Connect();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $postBody = file_get_contents('php://input');
    $decodedJson = json_decode($postBody, true);
    $token = $_GET["token"];
    $os = $_GET["os"];

    $query = "SELECT * FROM push_notifications_tokens WHERE push_notification_token='" . $decodedJson['token'] . "';";
    $query_result = $mysqlProxy->ExecuteQuery($query);
    $data = $mysqlProxy->FetchAll($query_result);

    if ($trace == 1) {
        echo "<pre>";
        print_r($data);
        echo "</pre><br>";
    }

    if (count($data)) {
        $query = "UPDATE push_notifications_tokens SET lastSeenOn='" . date('Y-m-d H:i:s') . "' WHERE push_notification_token='" . $decodedJson['token'] . "';";

        if ($trace == 1) {
            echo $query . "<br />";
        }

        $mysqlProxy->ExecuteQuery($query);
    } else {
        $query = "INSERT INTO push_notifications_tokens (`push_notification_token`, `lastSeenOn`, `os`) VALUES ('" . $decodedJson['token'] . "', '" . date('Y-m-d H:i:s') . "', '" . $decodedJson['os'] . "');";

        if ($trace == 1) {
            echo $query . "<br />";
        }

        $mysqlProxy->ExecuteQuery($query);
    }
} else {
    $query = "SELECT * FROM push_notifications_tokens;";

    if ($trace == 1) {
        echo $query . "<br />";
    }

    $query_result = $mysqlProxy->ExecuteQuery($query);
    $allRegistrations = $mysqlProxy->FetchAll($query_result);

    if ($trace == 1) {
        echo "<pre>";
        print_r($allRegistrations);
        echo "</pre><br>";
    }

    for ($i = 0; $i < count($allRegistrations); $i = $i + 500) {
        sendNotifications(array_slice($allRegistrations, $i, 500), $trace);
    }
}

$mysqlProxy->Disconnect();
