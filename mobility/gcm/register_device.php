<?php
date_default_timezone_set('Europe/Paris');
header('Content-Type: text/html; charset=UTF-8');

$trace = $_GET["trace"];
$device_id = $_GET["device_id"];

if ($trace != 1)
{
    // Désactiver le rapport d'erreurs
    error_reporting(0);
}

require_once "../const.php";
require_once("../PHP_DatabaseLib/MysqlProxy.php");

$mysqlProxy = new MysqlProxy(DATABASE_SERVER, DATABASE_USER, DATABASE_PASSWORD, MOBILITY_DATABASE_NAME);
$mysqlProxy->Connect();

$query = "SELECT * "
    . "FROM " . "gcm_devices "
    . "WHERE device_id=" . $device_id;

if ($trace == 1)
{
    echo "<pre>";
    echo $query . "<br />";
}

$query_result = $mysqlProxy->ExecuteQuery($query);
$device = $mysqlProxy->FetchAssoc($query_result);

if ($trace == 1)
{
    print_r($device);
}

if ($device == null)
{
    $query = "INSERT INTO gcm_devices"
        . "(device_id, register_date)"
        . "VALUES ('$device_id','" . date("c") . "')";

    if ($trace == 1)
    {
        echo $query . "<br />";
    }

    $mysqlProxy->ExecuteQuery($query);
}

$result["registered"] = 1;

$flags = JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_UNESCAPED_SLASHES;

$resultJSon = json_encode($result, $flags);
if ($trace == 1)
{
    require_once "../jsonHelper.php";
    $resultJSon = prettyPrint($resultJSon);
}

$mysqlProxy->Disconnect();

if ($trace == 1)
{
    echo "<pre>";
    print_r($resultJSon);
    echo "</pre>";
}
else
{
    echo $resultJSon;
}

?>
