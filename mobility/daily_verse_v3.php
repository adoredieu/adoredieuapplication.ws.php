<?php
require_once "cors.php";
cors();
header('Content-Type: text/html; charset=UTF-8');

$trace = $_GET["trace"];
$day = $_GET["day"];

if ($trace != 1) {
    // Désactiver le rapport d'erreurs
    error_reporting(0);
}

require_once "const.php";
require_once("PHP_DatabaseLib/MysqlProxyPhp7.php");

$mysqlProxy = new MysqlProxy(DATABASE_SERVER, DATABASE_USER, DATABASE_PASSWORD, MOBILITY_DATABASE_NAME);
$mysqlProxy->Connect();

$query = 'SET CHARACTER SET utf8';
$mysqlProxy->ExecuteQuery($query);

$dayNumber = 1;
$query = "SELECT * "
    . "FROM daily_verses_v2 "
    . "WHERE _id=$day";

if ($trace == 1) {
    echo $query . "<br />";
}

$query_result = $mysqlProxy->ExecuteQuery($query);

$data = $mysqlProxy->FetchAssoc($query_result);

$flags = JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_UNESCAPED_SLASHES;

$result = json_encode($data, $flags);
if ($trace == 1) {
    require_once "jsonHelper.php";
    $result = prettyPrint($result);
}

$mysqlProxy->Disconnect();

if ($trace == 1) {
    echo "<pre>";
    print_r($result);
    echo "</pre>";
} else {
    echo $result;
}

?>
